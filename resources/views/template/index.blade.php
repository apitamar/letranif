@extends('template.layout')

@section('contenido')

    <h1 class="page-header">Calcula letra DNI / NIF</h1>
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <form action="/action_page.php">
                <div class="form-group">
                    <label for="dni input-lg">Introduce los números del DNI y pulsa en calcular</label>
                    <input type="text" class="form-control input-lg" id="dni">
                </div>

                <div class="form-group">
                    <button type="button" class="btn btn-primary input-lg" id="calcular">CALCULAR LETRA</button>
                </div>

                <div class="form-group">
                    <label for="nif input-lg">Resultado</label>
                    <input type="text" class="form-control input-lg" id="nif">
                </div>
            </form>
        </div>
    </div>

    </div>

@endsection

@section('script-js')

    <script>
        var letras = {
            0: "T",
            1: "R",
            2: "W",
            3: "A",
            4: "G",
            5: "M",
            6: "Y",
            7: "F",
            8: "P",
            9: "D",
            10: "X",
            11: "B",
            12: "N",
            13: "J",
            14: "Z",
            15: "S",
            16: "Q",
            17: "V",
            18: "H",
            19: "L",
            20: "C",
            21: "K",
            22: "E"
        }

        $('#calcular').on('click', function() {
            var dni = $('#dni').val();
            var resto = dni % 23;

            $('#nif').val($('#dni').val()+letras[resto]);

        });
    </script>

@endsection




